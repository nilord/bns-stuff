(function(){
	var $table = $('<table><thead><tr><th>ID</th><th>Key</th><th>Skill</th><th>Group</th><th>Icon</th></tr></thead><tbody></tbody></table>');
	var $tbody = $table.children('tbody');
	
	$('ul.skill_list li').each(function(index){
		var $tr = $('<tr class="' + (['even', 'odd'][index % 2]) + '"></tr>');
		var $this = $(this);
		
		[
			$this.data('id'),
			$this.find('.skill_name img').prop('outerHTML'),
			$this.find('.skill_name').text().trim(),
			$this.find('.skill_group').prop('outerHTML'),
			$this.find('.skill_img_link img').prop('outerHTML')
		].forEach(function(html){
			$('<td></td>').html(html).appendTo($tr);
		});
		
		$tr.appendTo($tbody);
	});
	
	$('<style>' + [
		'table { color: #000; margin-top: 15px; width: 1067px; }',
		'th { padding: 10px; background: #555; color: #fff; }',
		'tr { text-align: center; }',
		'tbody > tr.odd{ background: #f5f5f5; }',
		'tbody > tr:hover { background: #f1f1f1; }',
		'tbody > tr.hidden { display: none; }',
		'input[type="search"]{padding: 5px; border: 1px solid #aaa; border-radius: 5px; margin: 20px 10px 10px; }'
	].join('') + '</style>').appendTo(document.body);
	
	$table.appendTo(document.body);
	
	// search
	$('<input type="search" placeholder="Search skills">').on('keyup', function(){
		var value = this.value.toLowerCase();
		var index = 0;
		
		$tbody.children('tr').each(function(){
			this.className = !value || (
				value && (
					~this.cells[0].textContent.toLowerCase().indexOf(value)
					|| ~this.cells[2].textContent.toLowerCase().indexOf(value)
				)
			) ? ['even', 'odd'][index++ % 2] : 'hidden';
		});
	}).insertBefore($table);
})();